export interface IEmployee {
    guid: string;
    firstName: string;
    lastName: string;
    middleName: string | null;
    managerId: string | null;
}
