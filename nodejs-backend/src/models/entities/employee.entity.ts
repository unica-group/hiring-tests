import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
/**
 * Сотрудник компании
 */
@Entity({ name: 'employees', schema: 'public' })
export class EmployeeEntity extends BaseEntity {

    @Column('uuid', {
        comment: 'Уникальный идентификатор',
        primary: true,
        nullable: false,
        unique: true,
        name: 'guid'
    })
    public guid: string;

    @Column('varchar', {
        comment: 'Имя',
        nullable: false,
        name: 'first_name',
    })
    public firstName: string;

    @Column('varchar', {
        comment: 'Фамилия',
        nullable: false,
        name: 'last_name',
    })
    public lastName: string;

    @Column('varchar', {
        comment: 'Отчество',
        nullable: true,
        name: 'middle_name',
    })
    public middleName: string | null;

    @Column('uuid', {
        comment: 'Идентификатор менеджера',
        nullable: true,
        name: 'manager_id'
    })
    public managerId: string | null;

    @ManyToOne(type => EmployeeEntity, employee => employee.subordinates)
    @JoinColumn({ name: 'manager_id' })
    public manager?: EmployeeEntity | null;

    @OneToMany(type => EmployeeEntity, employee => employee.manager)
    public subordinates?: EmployeeEntity[];
}
