import { IsNotEmpty, IsUUID } from 'class-validator';

export class EmployeesFindOneDTO {
    @IsNotEmpty()
    @IsUUID('4')
    public guid: string;
}
