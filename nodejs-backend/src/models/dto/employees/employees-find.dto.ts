import { IsNumber, IsOptional } from 'class-validator';

export class EmployeesFindDTO {
    @IsOptional()
    @IsNumber()
    public limit?: number;

    @IsOptional()
    @IsNumber()
    public offset?: number;
}
