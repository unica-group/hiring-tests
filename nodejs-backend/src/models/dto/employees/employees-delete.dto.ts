import { IsNotEmpty, IsUUID } from 'class-validator';

export class EmployeesDeleteDTO {
    @IsNotEmpty()
    @IsUUID('4')
    public guid: string;
}
