import { IsNotEmpty, IsOptional, IsString, IsUUID } from 'class-validator';
import { IsNullOrString, IsNullOrUUID } from 'src/services/class-validator';

export class EmployeesUpdateDTO {
    @IsNotEmpty()
    @IsUUID('4')
    public guid: string;

    @IsOptional()
    @IsString()
    public firstName?: string;

    @IsOptional()
    @IsString()
    public lastName?: string;

    @IsOptional()
    @IsNullOrString()
    public middleName?: string | null;

    @IsOptional()
    @IsNullOrUUID()
    public managerId?: string;
}

