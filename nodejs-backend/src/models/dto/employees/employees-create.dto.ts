import { IsNotEmpty, IsString, IsUUID } from 'class-validator';
import { IsNullOrString, IsNullOrUUID } from 'src/services/class-validator';

export class EmployeesCreateDTO {
    @IsNotEmpty()
    @IsUUID('4')
    public guid: string;

    @IsNotEmpty()
    @IsString()
    public firstName: string;

    @IsNotEmpty()
    @IsString()
    public lastName: string;

    @IsNotEmpty()
    @IsNullOrString()
    public middleName: string | null;

    @IsNotEmpty()
    @IsNullOrUUID()
    public managerId: string;
}

