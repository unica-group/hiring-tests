import { EmployeesCreateDTO, EmployeesDeleteDTO, EmployeesFindDTO, EmployeesFindOneDTO, EmployeesUpdateDTO } from '@dto';
import {
    Body,
    Delete,
    Get,
    JsonController,
    OnUndefined,
    Param,
    Post,
    Put,
    QueryParam
} from 'routing-controllers';
import { EmployeeService } from 'src/services/employee.service';
import { validateDTO } from './common';

@JsonController('/employees')
export class EmployeesController {
    constructor(private employeesService: EmployeeService) {}

    @Get('/')
    public async find(
        @QueryParam('limit') limit: number,
        @QueryParam('offset') offset: number,
    ) {
        const findDTO = new EmployeesFindDTO()
        findDTO.limit = limit;
        findDTO.offset = offset;

        await validateDTO(findDTO);

        return this.employeesService.find(findDTO);
    }

    @Get('/:guid')
    public async findOne(@Param('guid') guid: string) {
        const findOneDTO = new EmployeesFindOneDTO()
        findOneDTO.guid = guid;

        await validateDTO(findOneDTO);

        return this.employeesService.findOne(findOneDTO);
    }

    @Post('/:guid')
    public async create(@Param('guid') guid: string,
                        @Body() createDTO: EmployeesCreateDTO) {
        createDTO.guid = guid;

        await validateDTO(createDTO);

        return this.employeesService.create(createDTO);
    }

    @Put('/:guid')
    public async update(@Param('guid') guid: string,
                        @Body() updateDTO: EmployeesUpdateDTO) {
                            updateDTO.guid = guid;

        await validateDTO(updateDTO);

        return this.employeesService.update(updateDTO);
    }

    @Delete('/:guid')
    @OnUndefined(204)
    public async delete(@Param('guid') guid: string) {
        const deleteDTO = new EmployeesDeleteDTO();
        deleteDTO.guid = guid;

        await validateDTO(deleteDTO);

        return this.employeesService.delete(deleteDTO);
    }
}
