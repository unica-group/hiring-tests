import { EmployeeEntity } from "src/models/entities/employee.entity";
import { IEmployee } from "src/models/interfaces/employee.interface";
import {MigrationInterface, QueryRunner} from "typeorm";

export class initEmployees1627247225707 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {

        /*
                         Бакст
                        /     \
            Боровиковский      Васнецов
                              /    |   \
                      Верещагин  Гутов  Иванов
        */

        const employees: IEmployee[] = [
            {
                guid: '5dafd2be-3427-4634-8483-3d99cc57ae88',
                firstName: 'Лев',
                lastName: 'Бакст',
                middleName: 'Самойлович',
                managerId: null,
            },
            {
                guid: '31780530-d910-46d4-b30b-96860298b68d',
                firstName: 'Владимир',
                lastName: 'Боровиковский',
                middleName: 'Лукич',
                managerId: '5dafd2be-3427-4634-8483-3d99cc57ae88',
            },
            {
                guid: 'c8e5af77-8220-4e57-b7d9-c8736644c3b1',  
                firstName: 'Виктор',
                lastName: 'Васнецов',
                middleName: 'Михайлович',
                managerId: '5dafd2be-3427-4634-8483-3d99cc57ae88',
            },
            {
                guid: 'b18e581e-47da-4641-ad28-0385b950a98f', 
                firstName: 'Василий',
                lastName: 'Верещагин',
                middleName: 'Васильевич',
                managerId: 'c8e5af77-8220-4e57-b7d9-c8736644c3b1',
            },
            {
                guid: 'dd0b2e04-6f55-41e5-875f-1a372baaabbd',
                firstName: 'Дмитрий',
                lastName: 'Гутов',
                middleName: 'Геннадьевич',
                managerId: 'c8e5af77-8220-4e57-b7d9-c8736644c3b1',
            },
            {
                guid: '7d1c1fcb-047b-4075-8151-660b5c2b3290',
                firstName: 'Александр',    
                lastName: 'Иванов',
                middleName: 'Андреевич',
                managerId: 'c8e5af77-8220-4e57-b7d9-c8736644c3b1',
            },
        ];
        
        for (const employee of employees) {
            const middleName = employee.middleName ? `'${employee.middleName}'`: null;
            const managerId = employee.managerId ? `'${employee.managerId}'`: null;

            await queryRunner.query(`INSERT INTO "public"."employees" ( guid, first_name, last_name, middle_name, manager_id ) ` +
            `VALUES ( '${employee.guid}', '${employee.firstName}', '${employee.lastName}', ${middleName}, ${managerId} )`);
        }
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
