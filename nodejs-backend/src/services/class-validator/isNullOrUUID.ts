import { registerDecorator, ValidationArguments, ValidationOptions } from 'class-validator';
import { isUuid } from 'uuidv4';

export function IsNullOrUUID(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        validationOptions = {
            ...validationOptions,
            message: validationOptions?.message || 'Value should be uuid or null',
        }
        registerDecorator({
            name: 'isNullOrUUID',
            target: object.constructor,
            propertyName: propertyName,
            constraints: [],
            options: validationOptions,
            validator: {
                validate(value: any, args: ValidationArguments) {
                    return typeof value === 'string' && value.length > 0 && isUuid(value) || value === null;
                },
            },
        });
    };
}
