import { registerDecorator, ValidationArguments, ValidationOptions } from 'class-validator';

export function IsNullOrObject(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        validationOptions = {
            ...validationOptions,
            message: validationOptions?.message || 'Value should be object or null',
        }
        registerDecorator({
            name: 'isNullOrObject',
            target: object.constructor,
            propertyName: propertyName,
            constraints: [],
            options: validationOptions,
            validator: {
                validate(value: any, args: ValidationArguments) {
                    return !!value && typeof value === 'object' || value === null;
                },
            },
        });
    };
}
