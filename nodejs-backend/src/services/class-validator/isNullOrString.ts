import { registerDecorator, ValidationArguments, ValidationOptions } from 'class-validator';

export function IsNullOrString(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        validationOptions = {
            ...validationOptions,
            message: validationOptions?.message || 'Value should be string or null',
        }
        registerDecorator({
            name: 'isNullOrString',
            target: object.constructor,
            propertyName: propertyName,
            constraints: [],
            options: validationOptions,
            validator: {
                validate(value: any, args: ValidationArguments) {
                    return typeof value === 'string' && value.length > 0 || value === null;
                },
            },
        });
    };
}
