import { registerDecorator, ValidationArguments, ValidationOptions } from 'class-validator';

export function IsNullOrNumber(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        validationOptions = {
            ...validationOptions,
            message: validationOptions?.message || 'Value should be number or null',
        }
        registerDecorator({
            name: 'isNullOrNumber',
            target: object.constructor,
            propertyName: propertyName,
            constraints: [],
            options: validationOptions,
            validator: {
                validate(value: any, args: ValidationArguments) {
                    return typeof value === 'number' && !isNaN(value) || value === null;
                },
            },
        });
    };
}
