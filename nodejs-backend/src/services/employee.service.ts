import { EmployeesFindDTO, EmployeesFindOneDTO, EmployeesCreateDTO, EmployeesDeleteDTO, EmployeesUpdateDTO } from "@dto";
import { IEmployee } from "src/models/interfaces/employee.interface";
import { Service } from "typedi";

@Service()
export class EmployeeService {

    public async find(options: EmployeesFindDTO): Promise<IEmployee[]> {
        throw new Error('NOT_EMPLEMENTED');
    }

    public async findOne(options: EmployeesFindOneDTO): Promise<IEmployee> {
        throw new Error('NOT_EMPLEMENTED');
    }

    public async create(options: EmployeesCreateDTO): Promise<IEmployee> {
        throw new Error('NOT_EMPLEMENTED');
    }

    public async update(options: EmployeesUpdateDTO): Promise<IEmployee> {
        throw new Error('NOT_EMPLEMENTED');
    }

    public async delete(options: EmployeesDeleteDTO): Promise<void> {
        throw new Error('NOT_EMPLEMENTED');
    }
}
