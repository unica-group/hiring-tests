import {MigrationInterface, QueryRunner} from "typeorm";

export class initEntities1627246257106 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "public"."employees" ("guid" uuid NOT NULL, "first_name" character varying NOT NULL, "last_name" character varying NOT NULL, "middle_name" character varying, "manager_id" uuid, CONSTRAINT "UQ_f0dbfdc85de03ef530dc05b406f" UNIQUE ("guid"), CONSTRAINT "PK_f0dbfdc85de03ef530dc05b406f" PRIMARY KEY ("guid"))`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."employees" ADD CONSTRAINT "FK_c894a3d72f10c81ec45a18a1653" FOREIGN KEY ("manager_id") REFERENCES "public"."employees"("guid") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "public"."employees" DROP CONSTRAINT "FK_c894a3d72f10c81ec45a18a1653"`, undefined);
        await queryRunner.query(`DROP TABLE "public"."employees"`, undefined);
    }

}
